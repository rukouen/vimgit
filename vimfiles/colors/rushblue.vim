" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" Maintainer:	Kent Nguyen <rukouen@gmail.com>
" Last Change:	2012/12/29

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "rushblue"

hi Normal		guifg=#F4F4F4       ctermfg=White      gui=NONE cterm=NONE term=NONE
hi Number		guifg=#88E47E       ctermfg=LightBlue  gui=NONE cterm=NONE term=NONE
hi Comment		guifg=#848484       ctermfg=Grey       gui=NONE cterm=NONE term=NONE  
hi String		guifg=#F5EF63       ctermfg=Yellow     gui=NONE cterm=NONE term=NONE  
hi Constant		guifg=#F5A963       ctermfg=LightBlue  gui=NONE cterm=NONE term=NONE  
hi Special		guifg=#99CDF9       ctermfg=LightBlue  gui=NONE cterm=NONE term=NONE  
hi Identifier 	guifg=#99CDF9       ctermfg=LightGreen gui=NONE cterm=NONE term=NONE
hi Statement 	guifg=#99CDF9       ctermfg=LightGreen gui=NONE cterm=NONE term=NONE  		
hi PreProc		guifg=#F4F4F4       ctermfg=LightBlue  gui=NONE cterm=NONE term=NONE  		
hi Type			guifg=#99CDF9       ctermfg=LightGreen gui=NONE cterm=NONE term=NONE  		
hi Function		guifg=#99CDF9       ctermfg=LightGreen gui=NONE cterm=NONE term=NONE		
hi Operator		guifg=#99CDF9       ctermfg=LightGreen gui=NONE cterm=NONE term=NONE  
hi Ignore		guifg=Black         ctermfg=Black	   gui=NONE cterm=NONE term=NONE  
hi Error		guifg=#F4F4F4       guibg=#A5303C  ctermfg=White	   ctermbg=Red   gui=NONE cterm=NONE term=NONE  
hi Todo			guifg=Black         guibg=#F5EF63  ctermfg=Black 	   ctermbg=Yellow gui=NONE cterm=NONE term=NONE  
hi Exception	guifg=LightGreen    ctermfg=LightGreen gui=NONE cterm=NONE term=NONE  
hi Search   	guifg=White         guibg=#326FA2      gui=NONE cterm=NONE term=NONE  
hi VertSplit   	guifg=#000000       guibg=#666666      gui=NONE cterm=NONE term=NONE  
hi NonText   	guifg=#444444       guibg=#111111      gui=NONE cterm=NONE term=NONE  
hi CursorLine  	guibg=#232323       gui=NONE cterm=NONE term=NONE  
hi CursorLineNr	guifg=#EEEEEE       guibg=#555555       gui=Bold cterm=NONE term=NONE  

hi Title        guifg=White         ctermfg=White       gui=NONE cterm=NONE term=NONE  
hi Directory    guifg=LightGreen    ctermfg=LightGreen

hi SignColumn   guibg=#202020       gui=bold
hi LineNr		guifg=DarkGrey      guibg=#2A2A2A     ctermfg=DarkGrey	ctermbg=Black

hi Visual       guibg=#505050
hi MatchParen   guifg=#F4F4F4       guibg=#1E6865

" Common groups that link to default highlighting.
hi link Character	Constant
hi link Boolean		Identifier
hi link Float		Number
hi link Conditional	Statement
hi link Repeat		Statement
hi link Label		Statement
hi link Keyword		Statement
hi link Exception	Statement
hi link Include		PreProc
hi link Define		PreProc
hi link Macro		PreProc
hi link PreCondit	PreProc
hi link StorageClass	Type
hi link Structure	Type
hi link Typedef		Type
hi link Tag			Special
hi link SpecialChar	Special
hi link Delimiter	Special
hi link SpecialComment Special
hi link Debug		Special

" Indent Guides
hi IndentGuidesOdd	guibg=#1A1A1A ctermbg=Black
hi IndentGuidesEven	guibg=#1F1F1F ctermbg=Black

" Tab colouring
hi TabLine      ctermfg=LightGrey       ctermbg=DarkGrey    cterm=NONE gui=NONE cterm=NONE term=NONE
hi TabLineFill  ctermfg=Black           gui=NONE cterm=NONE term=NONE  
hi TabLineSel   ctermbg=DarkBlue        gui=NONE cterm=Bold term=NONE  

" Autocomplete menu
hi Pmenu        ctermbg=DarkGrey    guibg=#555555   guifg=#0F0F0F
hi PmenuSel     ctermbg=DarkBlue    guibg=#192E6B

" NERDTree
hi NERDTreePart     guifg=White

" Taglist
hi MyTaglistTitle       guibg=#0F0F0F     guifg=LightGreen    cterm=NONE gui=NONE cterm=NONE term=NONE
hi MyTagListFileName    guibg=#222222     guifg=LightGreen    cterm=NONE gui=NONE cterm=NONE term=NONE
hi FoldColumn           guifg=LightGreen  guibg=#0F0F0F       gui=NONE ctermfg=NONE ctermbg=NONE cterm=NONE
