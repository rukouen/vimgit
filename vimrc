" Copied from example vimrc ==================================
" test
" Use Vim settings, rather than Vi settings (much better!).
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
    set nobackup		" do not keep a backup file, use versions instead
else
    set backup		" keep a backup file
endif
set history=1000		" command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" Enable mouse
set mouse=a

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

" Enable file type detection.
filetype plugin indent on

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
au!

" For all text files set 'textwidth' to 78 characters.
autocmd! FileType text setlocal textwidth=78

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd! BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") |
\   exe "normal! g`\"" |
\ endif

augroup END

" always set autoindenting on
set autoindent

scriptencoding utf-8
set encoding=utf-8

" Config ======================================

" always start in current working directory
"set autochdir

" show line numbers
set number

" show partial commands
set showcmd

" tabbing
set shiftwidth=4
set tabstop=4
set expandtab
set softtabstop=4

colorscheme rushblue

" always show statusline
set laststatus=2

" set backup location
set backupdir=$HOME/vimfiles/temp

" default swap location
set directory^=$HOME/vimfiles/swap

" set leader key
"let mapleader=","
let mapleader="\<Space>"

" always show tab bar
set showtabline=2

" use global clipboard
set clipboard=unnamed

" indentation
"set noautoindent
"autocmd BufEnter * set nocindent
" set smartindent

" disable auto comment
autocmd! FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" disable html bold italic link underlines
let html_no_rendering = 1

" show long lines that are wrapped but not completely in view
set display=lastline

" wrap left and right buttons to previous/next lines
set whichwrap+=<,>,h,l,[,]

" do not auto-insert line breaks
set textwidth=0
set wrapmargin=0

" highlight trailing whitespace as errors
" actual regex = (?<=[^\s])(?<=\w)[ \t]\+$
match Error /[^ \t-]\@<=[ \t]\+$/

" case insensitive searches
set ignorecase

" use forward slash for windows paths
set shellslash

" preserve indents with I, rest is default
set cpoptions="aABceFsI"

set timeoutlen=500

" grep command, needs cygwin grep
set grepprg=grep\ -nH

" adds extra lines on edge of screen
set scrolloff=1

set ttyfast

" faster macros
set lazyredraw

" motion select words with certain characters
"set iskeyword+=-
set iskeyword+=$
autocmd FileType css,scss setlocal iskeyword+=-

" fix split locations
set splitbelow
set splitright

set shortmess+=I

" always open files in new tab
"au! BufAdd * tab sball

" default ctags filename
set tags=.tags;/

" breakindent options
silent! set breakindent
silent! set briopt=shift:4

" Plugins =====================================

" start up pathogen
call pathogen#infect()

" autostart indent guides
let g:indent_guides_enable_on_vim_startup = 1

" start neocomplcache
let g:neocomplcache_enable_at_startup = 1

" Set ultisnips triggers
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"


" nerdtree
"autocmd vimenter * NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
let NERDTreeDirArrows=0
let NERDTreeMinimalUI=1

" airline/powerline use fancy arrows
let g:airline_powerline_fonts = 1
"let g:Powerline_symbols = 'fancy'

" powerline setup - :PowerlineClearCache when changing settings
"let g:Powerline_symbols = 'compatible'
"let g:Powerline_dividers_override = ['', '', '', '']

" indent-guides settings
let g:indent_guides_auto_colors = 0
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

" easy motion
let g:EasyMotion_leader_key = '<Leader>'

" ignore folders
"set wildignore+=*/tmp/*,*/cache/*,*.so,*.swp,*.zip     " MacOSX/Linux
"set wildignore+=*\\tmp\\*,*\\cache\\*,*.swp,*.zip,*.exe  " Windows
set wildignore+=.tags
let g:ctrlp_custom_ignore = {
\ 'dir':  'tmp\|^log\|^logs\|vendor\/symfony\/symfony\|cache\|node_modules\|web\/images\|web\/uploads\|data\/app\/output\|web\/assets\|\v[\/]\.(git|hg|svn|sass\-cache)$',
\ 'file': '\v\.(exe|so|dll|jpg|jpeg|gif|png|zip|gz|tar)$',
\ }

" set max files to cache ctrlp
let g:ctrlp_max_files = 25000
let g:ctrlp_working_path_mode = 0
let g:ctrlp_show_hidden = 1
let g:ctrlp_cache_dir = $HOME . '/.cache/ctrlp'
let g:ctrlp_clear_cache_on_exit = 0
"let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' } " use py matcher plugin

" use ctrlp plugin if no autocomplete menu is open
inoremap <expr> <C-P> pumvisible() ? '<C-P>' : '<c-o>:CtrlP<cr>'

" js syntax libs used
let g:used_javascript_libs = 'jquery,backbone,underscore'

" ultisnips
let g:UltiSnipsSnippetDirectories = [expand($HOME) . '/vimgit/vimfiles/snippets/']

" neocomplcache
" move around in insert mode without inserting
let g:neocomplcache_enable_insert_char_pre = 1

" syntastic highlighting
let g:syntastic_enable_signs=1
let g:syntastic_auto_loc_lis=1
set statusline+=%#warningmsg#
"let g:syntastic_quiet_messages = { "level": "warnings" }
let g:syntastic_disabled_filetypes=['java']
let g:syntastic_java_checkers=['']

" Taglist on right hand side
let g:Tlist_Use_Right_Window = 1
"let g:Tlist_File_Fold_Auto_Close = 1 "remove fold bar
let g:Tlist_Exit_OnlyWindow = 1
let g:Tlist_Compact_Format = 1
let g:Tlist_Show_One_File = 1
let g:tlist_php_settings='php;f:function'
let g:tlist_js_settings='js;f:function'
let g:Tlist_Sort_Type = "name"

" expand v selections
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

" airline
"let g:airline_left_sep=''
"let g:airline_right_sep=''
"let g:airline#extensions#tabline#left_sep = ''
"let g:airline#extensions#tabline#left_alt_sep = ''
"let g:airline#extensions#tabline#right_sep = ''
"let g:airline#extensions#tabline#right_alt_sep = ''
let g:airline_section_warning = airline#section#create(['syntastic'])
let g:airline_skip_empty_sections = 1
let g:airline_theme = "rushblue"

" easymotion
let g:EasyMotion_do_mapping = 0 " Disable default mappings
map <Leader>g <Plug>(easymotion-s)

" gutentags
let g:gutentags_tagfile = '.tags'
let g:gutentags_cache_dir = expand($HOME) . '/vimfiles/ctags'

" semantic highlight colours
let g:semanticGUIColors = [ '#E6EE9C', '#FFE082', '#FFCC80', '#FFAB91', '#BCAAA4', '#B0BEC5',  '#FF8A65', '#F9BDBB', '#F9BDBB', '#E1BEE7', '#D1C4E9', '#FFE0B2', '#D0D9FF', '#A3E9A4', '#DCEDC8', '#C5E1A5', '#F0F4C3', '#BCFFBC', '#FFEBBC', '#FFD7BC', '#FFBCDE', '#F4BEFF', '#DAC2FF', '#C4BFDD', '#6387FF', '#B9FFB2', '#89AC9E' ]

" incsearch
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)

" Custom bindings ==============================

" extra escape binds
imap ii <Esc>
imap aa <Esc>
imap jk <Esc>
imap kj <Esc>

nmap <F2> :NERDTreeToggle<CR>:TlistToggle<CR>
imap <F2> <C-o>:NERDTreeToggle<CR>:TlistToggle<CR>
nmap <F12> :NERDTreeToggle<CR>:TlistToggle<CR>
imap <F12> <C-o>:NERDTreeToggle<CR>:TlistToggle<CR>
nmap <F9> :NERDTreeToggle<CR>:TlistToggle<CR>
imap <F9> <C-o>:NERDTreeToggle<CR>:TlistToggle<CR>

" keep indented lines with no text - if autocomplete is showing, select auto complete word
inoremap <expr> <CR> pumvisible() ? '<CR>' : '<space><BS><CR><space><BS>'
nnoremap o o<space><BS>
nnoremap O O<space><BS>
nnoremap S S<space><BS>

" highlight first autocomplete in menu
" inoremap <expr> <C-n> pumvisible() ? '<C-n>' : '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" bind typos cause cbf
command! WQ wq
command! Wq wq
command! W w
command! Q q

" Use CTRL-S for saving
noremap <C-s> :update<CR>:echo ""<CR>
vnoremap <C-s> <C-c>:update<CR>:echo ""<CR>
inoremap <C-s> <C-c>:update<CR>:echo ""<CR>
"noremap <C-s> :update<CR>:Errors<CR>:echo ""<CR>
"vnoremap <C-s> <C-c>:update<CR>:Errors<CR>:echo ""<CR>
"inoremap <C-s> <C-c>:update<CR>:Errors<CR>:echo ""<CR>

" move entire block up or down
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv

" make home go to first character or start of line
noremap <expr> <Home> (col('.') == matchend(getline('.'), '^\s*')+1 ? '0' : '^')
"noremap <expr> <End> (col('.') == match(getline('.'), '\s*$') ? '$' : 'g_')
"vnoremap <expr> <End> (col('.') == match(getline('.'), '\s*$') ? '$h' : 'g_')
imap <Home> <C-o><Home>
"imap <End> <C-o><End>

" paste multiple times without overwriting what was copied
xnoremap p pgvy`]l

vnoremap <silent> y y`]l
"vnoremap <silent> p p`]l
"nnoremap <silent> p p`]

" paste, indent pasted line and recopy
"nnoremap p p`[v`]ygv=`]
"vnoremap p p`[v`]ygv=`]
"xnoremap p p`[v`]ygv=`]

" re-center view on n
"nmap n nzz

" re-center view on G
nmap G Gzz


" fix/add auto close parentheses and quotes
"imap { {<CR>}<C-d><Up><End><CR>
" "imap ( ()<Left>
"imap [ []<Left>
" inoremap <expr> ) strpart(getline('.'), col('.')-1, 1) == ")" ? <Right> : ")"
"inoremap <expr> ] strpart(getline('.'), col('.')-1, 1) == "]" ? <Right> : "]"
"inoremap <expr> ' strpart(getline('.'), col('.')-1, 1) == "\'" ? <Right> : "\'\'<Left>"
"inoremap <expr> " strpart(getline('.'), col('.')-1, 1) == "\"" ? <Right> : "\"\"<Left>"

" remap ; to :
nnoremap ; :
vnoremap ; :

" open tags in split window
map <C-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>zz

" move up and down normally in wrapped lines
nnoremap <expr> j (strlen(getline('.')) >= 60 ? "gj" : "j")
nnoremap <expr> k (strlen(getline('.')) >= 60 ? "gk" : "k")
nnoremap <expr> <Down> (strlen(getline('.')) >= 60 ? "gj" : "j")
nnoremap <expr> <Up> (strlen(getline('.')) >= 60 ? "gk" : "k")

" Scroll up/down half page
nmap <PageDown> <C-d>
nmap <PageUp> <C-u>
imap <PageDown> <C-d>
imap <PageUp> <C-u>

" make tab indent
nnoremap <Tab> I<Tab><Esc>w
nnoremap <S-Tab> I<BS><Esc>w



" bind next previous in quickfix window
"autocmd! BufReadPost quickfix nnoremap <buffer> <n> :cnext

" search whole project
function! GlobalSearch (pattern, current)
    if a:current == "global"
        "exec 'vimgrep /' . a:pattern . '/gj % **/*.php **/*.txt'
        exec 'grep -Iir --exclude=".tags" --exclude-dir={.svn,.git,log,cache} "' . a:pattern . '"'
    elseif a:current == "current"
        exec 'grep -i "' . a:pattern . '" "' . expand('%:p') . '"'
    endif
    botright cwindow
endfunction

" search project
command! -nargs=1 S :tabnew|:silent call GlobalSearch(expand('<args>'), 'global')|:silent /<args>/

" search current file
command! -nargs=1 SC :silent call GlobalSearch(expand('<args>'), 'current')|:silent /<args>/

"remap Esc in normal mode to clear previous search
nnoremap <Esc> <Esc>:noh<CR>:echo ""<CR>

nnoremap <C-LeftMouse> <C-LeftMouse>zt


" center screen and blink search result
nnoremap <silent> n nzz:call HLNext(0.15)<cr>
nnoremap <silent> N Nzz:call HLNext(0.15)<cr>

function! HLNext (blinktime)
    let [bufnum, lnum, col, off] = getpos('.')
    let matchlen = strlen(matchstr(strpart(getline('.'),col-1),@/))
    let target_pat = '\c\%#'.@/
    let ring = matchadd('Todo', target_pat, 101)
    redraw
    exec 'sleep ' . float2nr(a:blinktime * 1000) . 'm'
    call matchdelete(ring)
    redraw
endfunction


" Auto functions ==============================

" window transparency
"let g:vimtweak_dll = globpath(&rtp, 'vimtweak.dll')
"function! BackgroundTransparency(v)
"    call libcallnr(g:vimtweak_dll, 'SetAlpha', 255-a:v)
"endfunction
"
"augroup transparent_windows
"  autocmd!
"  autocmd FocusGained * call BackgroundTransparency(10)
"  "autocmd FocusLost * call BackgroundTransparency(8)
"augroup END

" semantic highlighting for php files
autocmd! BufRead,BufWritePost   *.php     call SetSemantic()
function! SetSemantic()
    SemanticHighlight
    set autoindent
endfunction

" auto compile style.scss file on save
"au! BufWritePost style.scss silent !start /b cmd /c sass --update %:p

" fix syntax highlighting for html and js
au! FileType html autocmd BufEnter * :syntax sync fromstart

" reload vimrc files on save
augroup myvimrc
    au! BufWritePost .vimrc,_vimrc,vimrc,.gvimrc,_gvimrc,gvimrc so $HOME/vimgit/vimrc | if has('gui_running') | so $HOME/vimgit/gvimrc | endif
augroup END


" Leader commands ==============================

" change word under cursor
nmap <Leader>cc ciw

" clear search highlighting
map <Leader>/ :noh<CR>

" fix windows line endings
"nmap <Leader>wl :%s/\r/\r/g<CR>:%s/<C-v><CR>/\r/g<CR>:noh<CR>:echo "DOS line endings fixed."<CR>
nmap <Leader>wl :%s/\\r/\\r/g<CR>:noh<CR>:echo "DOS line endings fixed."<CR>

" delete windows line endings
nmap <Leader>wd :%s/<C-v><CR>//<CR>:noh<CR>:echo "DOS line endings deleted."<CR>

" fix blank line indentations
" nmap <Leader>fi mf:g/^$/normal xO<CR>:noh<CR>'f:echo "Fixed blank line indentations"<CR>
nmap <Leader>fi mf:g/^$/normal xO<CR>:noh<CR>'f:echo "Fixed blank line indentations"<CR>
"nnoremap = <Esc>mfgv=<Space>fi
"vnoremap = <Esc>mfgv=<Space>fi

" mysql filetype
nmap <Leader>fm :set ft=mysql<CR>i


" visual select file
nmap <Leader>v ggVG

" map vhosts extra
nmap <Leader>vh :tabnew<CR>:e C:/wamp/bin/apache/Apache2.4.9/conf/extra/httpd-vhosts.conf<CR>
"branch

" map gvimrc vimrc file
nmap <Leader>vg :tabnew<CR>:e $HOME/vimgit/gvimrc<CR>
nmap <Leader>vr :tabnew<CR>:e $HOME/vimgit/vimrc<CR>
nmap <Leader>vc :tabnew<CR>:e $HOME/vimfiles/colors/rushblue.vim<CR>
nmap <Leader>vu :exec "!start cmd /c git -C " . expand($HOME) . "/vimgit pull origin master & pause"<CR>
nmap <Leader>vw :set noshellslash<CR>:exec "silent !start explorer " . expand($HOME) . "\\\\vimgit"<CR>:set shellslash<CR>

" snippets shortcut
nmap <Leader>sj :tabnew<CR>:e $HOME/vimgit/vimfiles/snippets/javascript.snippets<CR>
nmap <Leader>sp :tabnew<CR>:e $HOME/vimgit/vimfiles/snippets/php.snippets<CR>
nmap <Leader>sm :tabnew<CR>:e $HOME/vimgit/vimfiles/snippets/mysql.snippets<CR>

" sass watch . for current file's folder
"nmap <Leader>sw :set noshellslash<CR>:!start cmd /c sass --watch %:p<CR>:set shellslash<CR>

" svn commands
nmap <Leader>su :set noshellslash<CR>:silent exec '!start TortoiseProc /command:update /path:"' . getcwd() . '"'<CR>:set shellslash<CR>:echo "SVN update " . getcwd()<CR>
nmap <Leader>sd :set noshellslash<CR>:silent exec '!start TortoiseProc /command:diff /path:' . expand('%:p')<CR>:set shellslash<CR>:echo "SVN diff " . expand('%:p')<CR>
nmap <Leader>sc :set noshellslash<CR>:silent exec '!start TortoiseProc /command:commit /path:"' . getcwd() . '"'<CR>:set shellslash<CR>:echo "SVN commit " . getcwd()<CR>

" Commenting blocks of code
"autocmd! FileType c,cpp,java,scala,javascript,php let b:comment_leader = '//'
"autocmd! FileType sh,ruby,python   let b:comment_leader = '# '
"autocmd! FileType mysql            let b:comment_leader = '-- '
"autocmd! FileType conf,fstab       let b:comment_leader = '# '
"autocmd! FileType vim              let b:comment_leader = '" '
"noremap <silent> <Leader>cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
"noremap <silent> <Leader>dc :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

" close global search
nmap <Leader>cs :ccl<CR>:lclose<CR><Space>/zz:echo ""<CR>

" nerd tree and taglist
nmap <Leader>nt :NERDTreeToggle<CR>:TlistToggle<CR>

" close tabs
nmap <Leader>cw :tabclose<CR>

" open cmder window in current working directory
nmap <Leader>cm :silent !C:/cmder/Cmder.exe /START %:p:h<CR>:echo getcwd()<CR>

" new tab
nnoremap <Leader>t :tabnew<CR>

" cordova android build
nmap <Leader>ba :!start build-android.bat<CR>

" open explorer for file and working directory
nmap <Leader>we :set noshellslash<CR>:silent !start explorer /select,"%:p"<CR>:set shellslash<CR>:echo expand('%:p')<CR>
nmap <Leader>ww :set noshellslash<CR>:silent exec '!start explorer "' . getcwd() . '"'<CR>:set shellslash<CR>:echo getcwd()<CR>
"nmap <Leader>ww :set noshellslash<CR>:silent !start explorer "%:p:h"<CR>:set shellslash<CR>:echo expand('%:p:h')<CR> " current working file directory

" Go to tab by number
noremap <Leader>1 1gt
noremap <Leader>2 2gt
noremap <Leader>3 3gt
noremap <Leader>4 4gt
noremap <Leader>5 5gt
noremap <Leader>6 6gt
noremap <Leader>7 7gt
noremap <Leader>8 8gt
noremap <Leader>9 9gt
noremap <Leader>0 :tablast<cr>

" Easymotion plugin
map <Leader>g <Plug>(easymotion-s)

" repeat q macro
noremap <Leader>q @q<cr>

" Yank entire buffer
nmap <Leader>y ggyG

" leader tab to switch tabs
nmap <Leader><Tab> :tabnext<CR>
nmap <Leader><S-Tab> :tabp<CR>

" close all other tabs and open empty tab
nmap <Leader>ct :tabo<CR>:tabnew<CR>:tabo<CR>

" open tag definition
"nmap <Leader>r <C-]>zz:TlistOpen<CR>
nmap <Leader>r :vsp <CR>:exec("tag ".expand("<cword>"))<CR>zt

" toggle colour semantic highlighting
nmap <Leader>sh :SemanticHighlightToggle<CR>


" jump to next f
nmap <Leader>; :
