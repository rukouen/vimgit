" Theme to mimic the default colorscheme of powerline
" Not 100% the same so it's powerline... ish.
"
" Differences from default powerline:
" * Paste indicator isn't colored different
" * Far right hand section matches the color of the mode indicator
"
" Differences from other airline themes:
" * No color differences when you're in a modified buffer
" * Visual mode only changes the mode section. Otherwise
"   it appears the same as normal mode

" Normal mode                                    " fg             & bg
let s:N1 = [ '#266C1F' , '#8FC772' , 22  , 148 ] " darkestgreen   & brightgreen
let s:N2 = [ '#666666' , '#2A2A2A' , 247 , 236 ] " gray8          & gray2
let s:N3 = [ '#F4F4F4' , '#161616' , 231 , 233 ] " white          & gray4

" Insert mode                                    " fg             & bg
let s:I1 = [ '#224C6F' , '#F4F4F4' , 23  , 231 ] " darkestcyan    & white
let s:I2 = [ '#F4F4F4' , '#224C6F' , 74  , 31  ] " darkcyan       & darkblue
let s:I3 = [ '#D6E7F5' , '#326FA2' , 117 , 24  ] " mediumcyan     & darkestblue

" Visual mode                                    " fg             & bg
let s:V1 = [ '#080808' , '#F5A963' , 232 , 214 ] " gray3          & brightestorange
let s:V2 = [ '#666666' , '#2A2A2A' , 247 , 236 ] " gray8          & gray2
let s:V3 = [ '#F4F4F4' , '#161616' , 231 , 233 ] " white          & gray4

" Replace mode                                   " fg             & bg
let s:RE = [ '#F4F4F4' , '#A5303C' , 231 , 160 ] " white          & brightred

let g:airline#themes#rushblue#palette = {}

let g:airline#themes#rushblue#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)

let g:airline#themes#rushblue#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)
let g:airline#themes#rushblue#palette.insert_replace = {
      \ 'airline_a': [ s:RE[0]   , s:I1[1]   , s:RE[1]   , s:I1[3]   , ''     ] }

let g:airline#themes#rushblue#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)
let g:airline#themes#rushblue#palette.visual_replace = {
      \ 'airline_a': [ s:V1[0]   , s:V1[1]   , s:V1[2]   , s:V1[3]   , ''     ] }

let g:airline#themes#rushblue#palette.replace = copy(airline#themes#rushblue#palette.normal)
let g:airline#themes#rushblue#palette.replace.airline_a = [ s:RE[0] , s:RE[1] , s:RE[2] , s:RE[3] , '' ]

let s:IA = [ s:N2[1] , s:N3[1] , s:N2[3] , s:N3[3] , '' ]
let g:airline#themes#rushblue#palette.inactive = airline#themes#generate_color_map(s:IA, s:IA, s:IA)


let g:airline#themes#rushblue#palette.ctrlp = airline#extensions#ctrlp#generate_color_map(
      \ [ '#224C6F' , '#224C6F' , 189 , 55  , ''     ],
      \ [ '#F4F4F4' , '#326FA2' , 231 , 98  , ''     ],
      \ [ '#224C6F' , '#F4F4F4' , 55  , 231 , 'bold' ])
