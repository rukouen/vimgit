" gVim Specific =========================================================

" gvim background black hack
highlight Normal     guifg=White guibg=#111111

" gvim font
"set gfn=Droid\ Sans\ Mono:h12
set gfn=Droid\ Sans\ Mono\ for\ Powerline\ P:h12

" gvim hide menus
set guioptions-=m
set guioptions-=T

" maximise gvim on startup
au! GUIEnter * simalt ~x

" set font rendering options
set renderoptions=type:directx,gamma:1.0,contrast:0.2,level:1.0,geom:1,renmode:5,taamode:1
